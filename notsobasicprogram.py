from unittest import TestCase, expectedFailure, main
#Import Selenium
from selenium import webdriver
from selenium.webdriver.common.by import By

class SeleniumTests(TestCase):

    def setUp(self):
        #Launch Firefox browser
        self.driver = webdriver.Firefox()
    
    def tearDown(self):
        #Close browser
        self.driver.quit()

    def test_get_example(self):
        #Navigate to example.com
        self.driver.get("https://example.com/")
        #Get the first h1 element from the root of the DOM tree
        element = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(element.text, 'Example Domain')

if __name__ == "__main__":
    main()
