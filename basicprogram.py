#Import Selenium
from selenium import webdriver
from selenium.webdriver.common.by import By

#Launch Firefox browser
driver = webdriver.Firefox()
#Navigate to example.com
driver.get("https://example.com/")
#Get the first h1 element from the root of the DOM tree
print(driver.find_element(By.TAG_NAME, 'h1').text)
#Close browser
driver.quit()
